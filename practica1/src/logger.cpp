#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//Lee del fichero cadena de texto

int main(int argc, char* argv[]) {

	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo", O_RDONLY);
	int salir=0;  //la cambiaremos cuando queramos salir del bucle
	int ret;

	while(salir==0)
	{
		char buff[200];
		ret=read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
		if((buff[0]=='-')||(ret==-1)) //Comprobacion de error
		{
			printf("----Tenis cerrado. Cerrando logger...--- \n");
			salir=1; //finaliza el while

		}

	}

	close(fd);

	unlink("/tmp/loggerfifo");
	
	return 0;

}
