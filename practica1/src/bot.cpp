#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"


  int main()

{

	int file;            

	DatosMemCompartida* pMemComp;  //puntero a la zona de memoria compartida

	char* proyeccion;              //puntero a la proyeccion



	//Apertura del fichero en modo lectura/escritura



	file=open("/tmp/datosBot.txt",O_RDWR);



	//Proyecto el fichero



	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);


	//Cierre del fichero

	close(file);


	//Puntero de memoria compartida apuntando a la proyeccion


	pMemComp=(DatosMemCompartida*)proyeccion;


	//condicion salida
	int salir=0;

	//Acciones de control de la raqueta
	
	while(salir==0)
 	{
		if (pMemComp->accion==5)
		{
			salir=1;
		}
		usleep(25000);  
		float posRaqueta;
		
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
	}



	//Se desmonta la proyeccion de memoria



	munmap(proyeccion,sizeof(*(pMemComp)));



}
